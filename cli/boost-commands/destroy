#!/usr/bin/env bash

if [[ $1 == "-h" ]] || [[ $1 == "--help" ]];then
  echo "This command destroyed all records associated with a channel. usage:"
  echo ""
  echo "  boost destroy -p +12223334444"
  echo ""
  echo "Valid options are:"
  echo "  -p : phone number of channel to be destroyed"
  echo "  -e : path to .env file (in dev, use .env.dev)";
  echo ""
  echo "Warning: This will permanently delete all data related to this channel!"
  exit 1
fi

pushd `pwd` > /dev/null # store current dir
cd `dirname "$0"` # cd to script path

while getopts ":p:e:" opt; do
  case "$opt" in
    p)
      phone_number="$OPTARG"
      ;;
    e)
      env_file="$OPTARG"
      ;;
  esac
done

# check env vars are properly defined
source ./_check-env
check-env ${env_file}

if [[ -z ${phone_number} ]]
then
  echo "> ERROR: you must provide a phone number after the -p flag to destroy"
  exit 1
elif [[ ! ${phone_number} =~ ^(\+[0-9]{9,15})$ ]];then
  echo "> ERROR: -p must be a valid phone number prefixed by a country code"
  exit 1
fi

curl -s -X DELETE \
     -H "Content-Type: application/json" \
     -H "Token: $SIGNALBOOST_API_TOKEN" \
     -d "{ \"phoneNumber\": \"$phone_number\" }" \
     https://${SIGNALBOOST_HOST_URL}/phoneNumbers | jq

popd > /dev/null # return to starting directory
